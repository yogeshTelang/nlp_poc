import numpy as np
from scipy.spatial import distance

# scipy distance can ONLY be calculated on a 1-D array
# scipy distance can't be calculated between an array and itself

a = np.random.rand(3, 4)
b = np.random.rand(3, 4)
e = np.random.rand(3000, 4000)
f = np.random.rand(3000, 4000)
x = np.random.uniform(0, 10, 100000)
y = np.random.uniform(0, 10, 100000)
print ("a: {}\nb: {}\n".format(a , b))
c = np.array((1, 2, 3, 4, 5))
d = np.array((3, 4, 5, 6, 7))
print ("c: {}\nd: {}\n".format(c, d))
print ("x: {}\ny: {}\n".format(x, y))
print ("-------------------------------------------------------------------------------")
dist_a = np.linalg.norm(a)
dist_b = np.linalg.norm(b)
dist_c = np.linalg.norm(c)
dist_d = np.linalg.norm(d)

dist_ab = np.linalg.norm(a - b)
dist_cd = np.linalg.norm(c - d)
dist_ef = np.linalg.norm(e - f)
dist_xy = np.linalg.norm(x - y)

scipy_dist_cd = distance.euclidean(c, d)
scipy_dist_chebyshev_cd = distance.chebyshev(c, d)
scipy_dist_manhattan_cd = distance.cityblock(c, d)

scipy_dist_xy = distance.euclidean(x, y)
scipy_dist_manhattan_xy = distance.cityblock(x, y)
scipy_dist_chebyshev_xy = distance.chebyshev(x, y)

print ("numpy euclidean distance between c & d is: {}".format(dist_cd))
print ("scipy euclidean distance between c & d is: {}".format(scipy_dist_cd))
print ("scipy chebyshev distance between c & d is: {}".format(scipy_dist_chebyshev_cd))
print ("scipy manhattan distance between c & d is: {}".format(scipy_dist_manhattan_cd))
print ("-------------------------------------------------------------------------------")
print ("numpy euclidean distance between x & y is: {}".format(dist_xy))

print ("scipy euclidean distance between x & y is: {}".format(scipy_dist_xy))

print ("scipy chebyshev distance between x & y is: {}".format(scipy_dist_chebyshev_xy))
print ("scipy manhattan distance between x & y is: {}".format(scipy_dist_manhattan_xy))

print ("-------------------------------------------------------------------------------")
