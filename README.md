# README #

### What is this repository for? ###

POC programs for performing various NLP tasks

### How do I get set up? ###

Install Scipy 

```
sudo pip install scipy
```

nltk - to download , follow the steps listed in the website: https://pythonprogramming.net/tokenizing-words-sentences-nltk-tutorial/

grammar_check, 3To2, LanguageTool - to download, follow the steps listed in the website: https://pypi.org/project/grammar-check/

Install numpy 

```
sudo pip install numpy
```

### How do I run the POC programs? ###

Open terminal

cd to the file location

```
python "POC_program_filename.py"
```